// studNo:2300706332
// regNo:23/U/06332/EVE


#include <stdio.h>
#include <stdlib.h>

// the general struct for a node
struct Node {
    int num;
    struct Node *next;

};

// implentation of printList

void printList(struct Node* head){
    struct Node* current = head;
    while(current != NULL){
        printf("%d,current->num");
        current=current->next;
    }
    printf("NULL");
};

// creating new node
struct Node* createNode(int num ){
    struct Node *newNode =(struct Node*)malloc(sizeof(struct Node));
    if (newNode==NULL){
         printf("Unable to allocate memory\n");
         exit(1);
    
     }
     newNode->num=num;
     newNode->next=NULL;
     return newNode;

    // printf{"%d\n , *x"}
};



// implementation of append

void append(struct Node **head, int num){
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->num=num;
    newNode->next=NULL;

    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *last = *head;
    while (last->next != NULL) {
        last = last -> next;
    }
    last->next = newNode;
}

// prepend implementation
void prepend(struct Node **head, int num){
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->num=num;
    newNode->next =*head;
    *head=newNode;
}

// deleteByKy implentation
void  deleteByKey(struct Node **head, int key){
    struct Node* temp = *head;
    struct Node* prev =NULL;

    if(temp != NULL && temp ->num == key){
        *head = temp ->next;
        free(temp);
        return;
    }
    while(temp != NULL && temp-> num !=key){
        prev == temp;
        temp = temp->next;
    }
    if(temp == NULL){
        printf("key not found in linked lists");
        return;

    }
    prev->next = temp->next;
    free(temp);
}



void deleteByValue(struct Node **head, int value) {
    struct Node* temp = *head;
    struct Node* prev = NULL;

    // Check if head node contains the value
    if (temp != NULL && temp->num == value) {
        *head = temp->next;
        free(temp);
        return;
    }

    // Search for the node with the given value
    while (temp != NULL && temp->num!= value) {
        prev = temp;
        temp = temp->next;
    }

    // If the value is not found
    if (temp == NULL) {
        printf("Value not found in the linked list.\n");
        return;
    }

    // Delete the node containing the value
    prev->next = temp->next;
    free(temp);
}


void insertAfterKey(struct Node **head, int key, int value) {
    struct Node* newNode = (struct Node*) malloc(sizeof(struct Node));
    newNode->num = value;
    
    struct Node* current = *head;
    while (current != NULL) {
        if (current->num == key) {
            newNode->next = current->next;
            current->next = newNode;
            return;
        }
        current = current->next;
    }
    
    printf("Key not found in the linked list");
}



void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *current = *head;
    
    while (current != NULL) {
        if (current->num == searchValue) {
            struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
            newNode->num = newValue;
            newNode->next = current->next;
            current->next = newNode;
            return;
        }
        current = current->next;
    }
    
    printf("Value %d not found in the linked list", searchValue);
}




int main()
{
    struct Node *head = NULL;
    int choice, data;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        
        
        
        switch(choice){
            case 1:
               printList(head);
               break;
        
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
        
            case 3:
                printf("Invalid choice. Please try again.\n");
                break;
        
            case 4:
                printf("Delete function not implemented.\n");
            case 5:
                exit(0);
            default:
                printf("invalid choice.Please try again.\n");
        }
    }
    return 0;
}

